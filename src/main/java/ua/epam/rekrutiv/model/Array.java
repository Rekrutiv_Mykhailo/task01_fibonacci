package ua.epam.rekrutiv.model;

import java.util.ArrayList;
import java.util.List;

public class Array {


    public static int[] generateArray(Limit limit) {
        int interwal = limit.getEnd() - limit.getStart();

        int[] array = new int[interwal];
        for (int i = 0; i < interwal; i++) {
            array[i] = limit.getStart() + i;
        }

        return array;
    }

    public static int[] checkArrayOnAddNumbers(int[] array) {

        List<Integer> listOfAddNumbers = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                listOfAddNumbers.add(array[i]);
            }
        }
        return getInts(listOfAddNumbers);
    }

    public static int[] checkArrayOnEvenNumbers(int[] arrayOfNumbers) {

        List<Integer> listOfEvenNumbers = new ArrayList<Integer>();
        for (int i = 0; i < arrayOfNumbers.length; i++) {
            if (arrayOfNumbers[i] % 2 == 0) {
                listOfEvenNumbers.add(arrayOfNumbers[i]);
            }
        }
        return getInts(listOfEvenNumbers);
    }

    private static int[] getInts(List<Integer> listOfEvenNumbers) {
        int[] arrayOfEven;
        arrayOfEven = new int[listOfEvenNumbers.size()];
        for (int i = 0; i < listOfEvenNumbers.size(); i++) {
            arrayOfEven[i] = listOfEvenNumbers.get(i);
        }
        return arrayOfEven;
    }

    public static int sumNumbers(int[] arrayOfNumbers) {
        int sum = 0;
        for (int i = 0; i < arrayOfNumbers.length; i++) {
            sum += arrayOfNumbers[i];
        }
        return sum;
    }

    public static int[] createFibonachiArray(int sizeOfSet) {
        int[] fibArray = new int[sizeOfSet];
        fibArray[0] = 1;
        fibArray[1] = 1;
        for (int i = 2; i < sizeOfSet; i++) {
            fibArray[i] = fibArray[i - 2] + fibArray[i - 1];
        }
        return fibArray;
    }

    public static int[] findBiggestOddAndEven(int[] arrayOfNumbers) {
        int oddNumber[] = new int[2];
        for (int i = 0; i < arrayOfNumbers.length; i++) {
            if (arrayOfNumbers[i] % 2 != 0) {
                oddNumber[0] = arrayOfNumbers[i];
            } else if (arrayOfNumbers[i] % 2 == 0) {
                oddNumber[1] = arrayOfNumbers[i];
            }
        }
        return oddNumber;
    }

    public static void
    calculatePercentageOfAddAndEvenNumbers(int setOfSize) {
        double[] percent = new double[2];
        int[] fibArray = createFibonachiArray(setOfSize);
        int[] oddNumbs = checkArrayOnAddNumbers(createFibonachiArray(setOfSize));
        int[] evenNumbs = checkArrayOnEvenNumbers(createFibonachiArray(setOfSize));
        int countOfNumbs = oddNumbs.length + evenNumbs.length;
        percent[0] =((double) (oddNumbs.length) / countOfNumbs) * 100;
        percent[1] = ((double)(evenNumbs.length) / countOfNumbs) * 100;
        System.out.println("Percent of add numbs: " + percent[0] + "  Percent of even numbs: " + percent[1]);

    }

    public static void printArray(int array[]) {
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void printArrayBackward(int array[]) {
        System.out.println();
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }

}
