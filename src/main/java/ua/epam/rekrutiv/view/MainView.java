package ua.epam.rekrutiv.view;

import ua.epam.rekrutiv.controller.MainController;
import ua.epam.rekrutiv.model.Constant;
import ua.epam.rekrutiv.model.Limit;
import ua.epam.rekrutiv.model.MyException;

import java.util.Scanner;


public class MainView {
    public static void enterLimit() throws Exception {
        int start = 0;
        int end = 0;
        int size;
        String message;
        Limit limit;
        Scanner inSt = new Scanner(System.in);
        System.out.println(Constant.helloFrase);
        System.out.println(Constant.startOfLimit);
        try {
            start = inputIntData();
            System.out.println(Constant.endOfLimit);
            end = inputIntData();
        }
        catch (MyException e){
            System.err.println(e);
            enterLimit();
        }


        limit = new Limit(start, end);
        MainController.startFirstTask(limit);
        System.out.println(Constant.fibSize);
        size=inputIntData();
        System.out.println(Constant.option);
        message = inSt.nextLine();
        MainController.startSecondTask(size,message);
    }

    public static int inputIntData() throws MyException {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt())
            return scanner.nextInt();
        else {
            throw new MyException("it is incorrect input");
        }

    }
    public static void main(String[] args) throws Exception {
        enterLimit();
    }
}
