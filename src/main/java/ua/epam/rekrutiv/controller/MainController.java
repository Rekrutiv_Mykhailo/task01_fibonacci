package ua.epam.rekrutiv.controller;

import ua.epam.rekrutiv.model.Array;
import ua.epam.rekrutiv.model.Limit;

public class MainController {
    public static void startFirstTask(Limit limit) {
        int[] array = Array.generateArray(limit);
        Array.printArray(Array.checkArrayOnAddNumbers(array));
        Array.printArrayBackward(Array.checkArrayOnEvenNumbers(array));
        System.out.println();
        System.out.println(Array.sumNumbers(Array.checkArrayOnAddNumbers(array)));
        System.out.println(Array.sumNumbers(Array.checkArrayOnEvenNumbers(array)));
    }

    public static void startSecondTask(int size, String message) {
        int[] fibonachi = Array.createFibonachiArray(size);
        if (message == "1") {
            System.out.println(Array
                    .findBiggestOddAndEven(fibonachi)[1]);
        } else if (message == "2") {
            System.out.println(Array
                    .findBiggestOddAndEven(fibonachi)[2]);
        }
        Array.calculatePercentageOfAddAndEvenNumbers(size);

    }
}
